<!--
 *  =================================================
 *  Developers by fss
 *  Author      : RIZKI RAMDANI
 *  Email       : rizkiiramdaniii@gmail.com
 *  IG          : https://www.instagram.com/rizkiiramdani/
 *  Last Update : 17 September 2019
 *  =================================================
 -->
<?php 
    if ($this->session->level=='')
    {
        redirect(base_url());
    }else{
?>
<!DOCTYPE html>
<html>
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Aplikasi Pedagang Pasar</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/plugins/daterangepicker/daterangepicker-bs3.css">
    <style type="text/css"> .files{ position:absolute; z-index:2; top:0; left:0; filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; opacity:0; background-color:transparent; color:transparent; } </style>
    <script type="text/javascript" src="<?php echo base_url(); ?>/asset/admin/plugins/jQuery/jquery-1.12.3.min.js"></script>
    <script src="<?php echo base_url(''); ?>asset/ckeditor/ckeditor.js"></script>
    <style type="text/css">#example thead tr, #table1 thead tr, #example1 thead tr, #example2 thead tr{ background-color: #e3e3e3; } .checkbox-scroll { border:1px solid #ccc; width:100%; height: 114px; padding-left:8px; overflow-y: scroll; }</style>
    <link rel="stylesheet" href="https://almsaeedstudio.com/themes/AdminLTE/plugins/pace/pace.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->

    <link rel="shortcut icon" href="<?php echo base_url(); ?>" />
    <style type="text/css">
        body{
          background-color: #222D32;
        }
        .borderless td, .borderless th {
            border: none !important;
        }
        .full-width{
            width : 100% !important; 
        }
        .text-white{
            color : #ffffff !important;
        }
        .cursor-pointer{
            cursor: pointer !important;
        }
        .steps-active{
            background-color: #0099E6 !important;
            border: 1px solid #0099E6;
            color:#fff;
        }
        .center-align {
            text-align: center;
        }
    </style>
	</head>

	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
            <?php include "include/header.php"; ?>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>asset/foto_user/blank.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                        <p>Alexander Pierce</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li><a href="<?php echo base_url() ?>dashboard/home"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                        
                        <li class="treeview">
                            <a href="#"><i class="fa fa-th-large"></i> <span>Master Data</span><i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php
                                    if($this->session->usergroups_id==''){
                                        echo "<li><a href='".base_url()."management/business_type'><i class='fa fa-circle-o'></i> Jenis Usaha</a></li>";
                                        echo "<li><a href='".base_url()."management/business_group'><i class='fa fa-circle-o'></i> Kelompok Usaha</a></li>";
                                        echo "<li><a href='".base_url()."management/floor'><i class='fa fa-circle-o'></i> Lantai</a></li>";
                                        echo "<li><a href='".base_url()."management/trash_type'><i class='fa fa-circle-o'></i> Jenis Sampah</a></li>";
                                        echo "<li><a href='".base_url()."management/ownership'><i class='fa fa-circle-o'></i> Kepemilikan</a></li>";
                                        echo "<li><a href='".base_url()."management/store_type'><i class='fa fa-circle-o'></i> Tipe Toko</a></li>";
                                        echo "<li><a href='".base_url()."management/hpt'><i class='fa fa-circle-o'></i> HPT</a></li>";
                                        echo "<li><a href='".base_url()."management/her'><i class='fa fa-circle-o'></i> HER</a></li>";
                                    }
                                ?>
                            </ul>
                        </li>
                        <?php
                            if($this->session->usergroups_id==''){
                                echo "<li><a href='".base_url()."market'><i class='fa fa-shopping-basket'></i> Pasar</a></li>";
                                echo "<li><a href='".base_url()."management/users'><i class='fa fa-users'></i> User Management</a></li>";
                            }
                        ?>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-print"></i> <span>Laporan</span><i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <?php
                                    if($this->session->usergroups_id==''){
                                        echo "<li><a href='".base_url()."report/trader'><i class='fa fa-circle-o'></i> Data Pedagang</a></li>";
                                    }
                                ?>
                            </ul>
                        </li>
                        <?php
                            if($this->session->usergroups_id==''){
                                echo "<li><a href='".base_url()."auth/logout'><i class='fa fa-power-off'></i> Sign out</a></li>";
                            }
                        ?>
                    </ul>
                </section>
            </aside>
            
            <?php echo $contents; ?>
		  	<footer class="main-footer">
			    <div class="pull-right hidden-xs">
			      	<b>Version</b> 0.1
			    </div>
			    <strong>Copyright &copy; <?php echo date('Y'); ?>  <a href="#" target="_blank">Aplikasi Database Pedagang Pasar</a>.</strong> All rights
			    reserved.
                <p>Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'Build With <i class="fa fa-heart"></i> CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>

			</footer>
		<div class="control-sidebar-bg"></div>
	</body>

	<script src="asset/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>$.widget.bridge('uibutton', $.ui.button);</script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://almsaeedstudio.com/themes/AdminLTE/plugins/pace/pace.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/morris/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/knob/jquery.knob.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/plugins/fastclick/fastclick.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/dist/js/app.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/dist/js/jquery.nestable.js"></script>
    <script>
        $('#rangepicker').daterangepicker();
        $('.datepicker').datepicker();
        $(function () { 
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
            });
        });
    </script>

    <script type="text/javascript">
        // To make Pace works on Ajax calls
        $(document).ajaxStart(function() { Pace.restart(); });
        $('.ajax').click(function(){
            $.ajax({url: '#', success: function(result){
                $('.ajax-content').html('<hr>Ajax Request Completed !');
            }});
        });
        /** add active class and stay opened when selected */
        var url = window.location;
        // for sidebar menu entirely but not cover treeview
        $('ul.sidebar-menu a').filter(function() {
        return this.href == url;
        }).parent().addClass('active');
        // for treeview
        $('ul.treeview-menu a').filter(function() {
        return this.href == url;
        }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
    </script>
</html>
<?php } ?>