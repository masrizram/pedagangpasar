<?php

    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Market extends CI_Controller {
        /**
            * Market / Index Page for this controller.
        */
        public function index()
        {
            $data['record'] = $this->model_apps->view_ordering('ref_pasar','JENISPASAR_ID','DESC');
            $this->template->load('template','market/index',$data);
        }

        public function detail()
        {
            $data['record'] = $this->model_apps->view_ordering('dat_potensi_retribusi','PASAR_ID','DESC');
            $this->template->load('template','market/detail',$data);
        }
    }